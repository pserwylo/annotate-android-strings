This is a work in progress proof of concept, to automatically annotate android screenshots containing strings, then upload them to Weblate.

Currently it is capable of:
* Dumping the UI hierarchy and a screenshot of the running Android device.
* Processing the UI hierarchy, and finding any strings froma corresponding `res/strings.xml` file
* Outputting one annotated screenshot for each string found.

Limitations:
* Needs a nicer UI to be able to be usable.
* Doesn't connect with espresso tests to periodically screenshot and annotate.
* Doesn't upload images to Weblate (or other systems?) yet.
* Doesn't annotate 'content-description' strings (used for accessibility).

## Example usage

1. Connect a physical device (or an emulator [without hardware acceleration](https://stackoverflow.com/questions/15892305/how-to-take-screenshot-in-android-emulator#comment44944356_22113986)).
2. Open your app on the device.
3. Navigate to a screen that has strings you want to screenshot then annotate.
4. Run `python3 run.py path/to/res/strings.xml path/to/output/dir`
5. Repeat steps 3 - 4 as many times as required.
6. For each string identified, a new annotated screenshot will be written to `path/to/output/dir/android.string.context.STRING_RES.png`.

## Dumping the UI from a running Android device

1. Connect a physical device (or an emulator [without hardware acceleration](https://stackoverflow.com/questions/15892305/how-to-take-screenshot-in-android-emulator#comment44944356_22113986)).
2. Run `python3 dump.py`

This will:
 * Take a screenshot and save it in `/tmp/screenshot-for-annotation.png`
 * Dump the UI heirarchy of the current screen in XML format to `/tmp/screenshot.xml`
 * Dump a prettified version of this XML into `/tmp/screenshot.pretty.xml`

What it **wont** do:
 * It doesn't yet do anything with this info. See below for details of how the next stages are expected to work.

### Example output from device

#### UI XML Dump

The dumped XML in [`example/screenshot.xml`](./example/screenshot.xml) contains nodes for each UI widget on the screen.
For the purposes of annotation, we are interested in the XML nodes with text in them, such as:

```
<node
  bounds="[0,794][425,867]"
  content-desc="Category Development"
  text="Development"
  ... />
<node
  bounds="[706,72][1080,237]"
  content-desc="View all 129 apps from the Connectivity category"
  text="VIEW ALL 129"
  ... />
```

#### Screenshot

[![](examples/screenshot-for-annotation.small.png)](examples/screenshot-for-annotation.png)

## Annotating dumped screenshot

To annotate, run `python3 annotate.py`.

This will annotate the image in [`examples/annotated-screenshot.png`](exampless/annotated-screenshot.png) and output:

[![](examples/annotated-screenshots/android.string.context.button_view_all_apps_in_category.small.png)](examples/annotated-screenshots/android.string.context.button_view_all_apps_in_category.png)
[![](examples/annotated-screenshots/android.string.context.category_Connectivity.small.png)](examples/annotated-screenshots/android.string.context.category_Connectivity.png)
[![](examples/annotated-screenshots/android.string.context.category_Development.small.png)](examples/annotated-screenshots/android.string.context.category_Development.png)
[![](examples/annotated-screenshots/android.string.context.category_Games.small.png)](examples/annotated-screenshots/android.string.context.category_Games.png)
[![](examples/annotated-screenshots/android.string.context.main_menu__categories.small.png)](examples/annotated-screenshots/android.string.context.main_menu__categories.png)
[![](examples/annotated-screenshots/android.string.context.main_menu__latest_apps.small.png)](examples/annotated-screenshots/android.string.context.main_menu__latest_apps.png)
[![](examples/annotated-screenshots/android.string.context.main_menu__swap_nearby.small.png)](examples/annotated-screenshots/android.string.context.main_menu__swap_nearby.png)
[![](examples/annotated-screenshots/android.string.context.menu_settings.small.png)](examples/annotated-screenshots/android.string.context.menu_settings.png)
[![](examples/annotated-screenshots/android.string.context.updates.small.png)](examples/annotated-screenshots/android.string.context.updates.png)
