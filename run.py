import logging
from sys import argv
import os

import annotate
import dump


def dump_and_annotate(strings_path, output_dir, keep_dump=False):
    screenshot = os.path.join(output_dir, "screenshot.png")
    xml = os.path.join(output_dir, "screenshot.xml")
    pretty_xml = os.path.join(output_dir, "screenshot.pretty.xml")

    dump.dump_info_from_device(screenshot, xml, pretty_xml)
    annotate.annotate_translateable_strings(xml, strings_path, screenshot, output_dir)

    if keep_dump:
        logging.debug("Keeping screenshot, XML, and prettified XML lying around.")
    else:
        logging.debug("Removing sscreenshot, XML, and prettified XML.")
        os.unlink(screenshot)
        os.unlink(xml)
        os.unlink(pretty_xml)


def print_usage():
    print("Usage: python3 run.py path_to_strings.xml output_dir [--keep-dump]")


if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG)

    if len(argv) != 3 and len(argv) != 4:
        logging.error("Incorrect number of arguments, expected 2 or 3 (path_to_strings.xml, output_dir, and perhaps --keep-dump).")
        print_usage()
        exit(1)

    strings_path = argv[1]
    if not os.path.isfile(strings_path):
        logging.error("\"%s\" should be the path to a valid Android strings.xml resource file." % strings_path)
        print_usage()
        exit(1)

    output_dir = argv[2]
    if not os.path.isdir(output_dir):
        logging.error("\"%s\" should be a directory." % output_dir)

    keep_dump = False
    if len(argv) == 4:
        if argv[3] != '--keep-dump':
            logging.error("Expected last argument to be --keep-dump, not \"%s\"." % argv[3])
            print_usage()
            exit(1)

        keep_dump = True

    dump_and_annotate(strings_path, output_dir, keep_dump=keep_dump)