"""
Dumps the UI of a connected android device.
Once dumped, annotates the screenshot with each string it finds in that screenshot.
"""

from xml.dom import minidom

from wand.drawing import Drawing
from wand.image import Image
from wand.color import Color

import re
import logging
import os


class StringInfo:

    def __init__(self, node, string_res):
        self.string_val = node.getAttribute('text')
        self.string_res = string_res

        bounds = re.match("\[(\d+),(\d+)\]\[(\d+),(\d+)\]", node.getAttribute('bounds'))
        self.left = int(bounds[1])
        self.top = int(bounds[2])
        self.right = int(bounds[3])
        self.bottom = int(bounds[4])


def annotate_translateable_strings(ui_xml_path, strings_xml_path, screenshot_path, out_dir):

    foreground_color = Color('red')
    background_color = Color('white')
    highlight_opacity = 0.3
    font_size = 48
    padding = 8

    nodes_to_annotate = find_translateable_strings(ui_xml_path, strings_xml_path)

    with Image(filename = screenshot_path) as img:
        for node in nodes_to_annotate:
            with img.clone() as i:

                logging.debug("Annotating @string/%s - \"%s\" - [%s, %s, %s, %s]" % (
                    node.string_res, node.string_res, node.left, node.top, node.right, node.bottom))

                # Draw a border around the English text in the screenshot.
                with Drawing() as draw:
                    draw.stroke_color = background_color
                    draw.stroke_width = 6
                    draw.fill_color = Color('none')
                    draw.rectangle(node.left - padding - 3, node.top - padding - 3, node.right + padding + 4, node.bottom + padding + 4)
                    draw.draw(i)

                # Draw a highlighted box around the text inside our nice border.
                with Drawing() as draw:
                    draw.stroke_color = foreground_color
                    draw.stroke_width = 3
                    draw.fill_color = foreground_color
                    draw.fill_opacity = highlight_opacity
                    draw.rectangle(node.left - padding, node.top - padding, node.right + padding, node.bottom + padding)
                    draw.draw(i)

                # Draw a border around the text we are about to annotate onto the screen.
                with Drawing() as draw:
                    draw.stroke_color = background_color
                    draw.fill_color = background_color
                    draw.font_size = font_size
                    draw.stroke_width = 8
                    draw.text(_calc_text_left(i, draw, node, node.string_res, padding), _calc_text_bottom(i, node, font_size, padding), node.string_res)
                    draw.draw(i)

                # Add the annotated text onto the screen.
                with Drawing() as draw:
                    draw.fill_color = foreground_color
                    draw.font_size = font_size
                    draw.text(_calc_text_left(i, draw, node, node.string_res, padding), _calc_text_bottom(i, node, font_size, padding), node.string_res)
                    draw.draw(i)

                filename = "android.string.context.%s.png" % node.string_res
                logging.info("Saved image for @string/%s to %s " % (node.string_res, filename))
                i.save(filename=os.path.join(out_dir, filename))


def _calc_text_bottom(image, node, font_size, padding):
    string_bottom = node.bottom + font_size + padding
    if string_bottom > image.height:
        string_bottom = node.top + padding

    return int(string_bottom)


def _calc_text_left(image, draw, node, text, padding):
    width = node.right - node.left
    middle = node.left + width / 2

    text_size = draw.get_font_metrics(image, text)
    left = middle - text_size.text_width / 2

    if left < padding:
        left = padding
    elif left + text_size.text_width > image.width:
        left = image.width - text_size.text_width - padding

    return int(left)


def find_translateable_strings(ui_xml_path, strings_xml_path):
    logging.info('Extracting translateable strings from UI')

    text_nodes = find_tags(ui_xml_path)

    strings_plurals = find_res_strings(strings_xml_path)
    strings = strings_plurals['strings']
    plurals = strings_plurals['plurals']

    nodes_to_annotate = []

    for node in text_nodes:
        text = node.getAttribute('text')
        logging.debug("Finding matching string resource for text \"%s\"" % text)
        string_res = match_strings(text, strings)
        if string_res is not None:
            string_res_name = string_res.getAttribute('name')
            logging.debug("Found string @string/%s" % string_res_name)
            nodes_to_annotate.append(StringInfo(node, string_res_name))
            continue

        plural_node = match_plurals(text, plurals)
        if plural_node is not None:
            plural_res_name = plural_node.getAttribute('name')
            logging.debug("Found plural @string/%s" % plural_res_name)
            nodes_to_annotate.append(StringInfo(node, plural_res_name))
            continue

        # Only a warning, because there will be many strings that originate from metadata, not from strings.xml
        logging.warn("Couldn't find string resource for text \"%s\"" % text)

    return nodes_to_annotate

def fuzzy_match(text, string_res):
    regex = re.sub('(%%|%[^%](\$.)?)', 'REPLACE_WITH_WILDCARD', string_res)
    regex = re.escape(regex).replace('REPLACE_WITH_WILDCARD', '.*')
    pattern = re.compile(regex, re.IGNORECASE)
    match = pattern.match(text)
    return match is not None


def match_plurals(text, plural_nodes):
    for plural_node in plural_nodes:
        for item_node in plural_node.getElementsByTagName('item'):
            item_value = item_node.firstChild.nodeValue
            if text == item_value or fuzzy_match(text, item_value):
                return plural_node

    return None


def match_strings(text, string_nodes):
    for node in string_nodes:
        string_value = node.firstChild.nodeValue
        if text == string_value or fuzzy_match(text, string_value):
            return node

    return None


def find_res_strings(strings_xml_path):
    logging.info('Finding English strings and plurals')
    xml = minidom.parse(strings_xml_path)
    string_nodes = xml.getElementsByTagName('string')
    plural_nodes = xml.getElementsByTagName('plurals')

    return {
        'strings': string_nodes,
        'plurals': plural_nodes
    }


def find_tags(ui_xml_path):
    logging.info("Searching for text nodes in UI")
    xml = minidom.parse(ui_xml_path)
    nodes = xml.getElementsByTagName('node')
    text_nodes = []
    for node in nodes:
        text = node.getAttribute('text')
        if len(text) > 0:
            logging.info('Found text on screen: %s' % text)
            text_nodes.append(node)

    return text_nodes


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    ui_xml_path = "./examples/screenshot.xml"
    strings_xml_path = "./examples/strings.xml"
    screenshot_path = "./examples/screenshot-for-annotation.png"
    out_dir = "/tmp"
    annotate_translateable_strings(ui_xml_path, strings_xml_path, screenshot_path, out_dir)
