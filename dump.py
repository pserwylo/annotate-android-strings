"""
Dumps the UI of a connected android device.
Once dumped, annotates the screenshot with each string it finds in that screenshot.
"""

from subprocess import check_output
from xml.dom import minidom

import re
import logging


def adb(command, *args):
    to_call = ["adb", command]
    to_call.extend(args)
    logging.debug("Executing adb command: %s", to_call)
    response = check_output(to_call)

    if len(response) > 0:
        logging.debug("Response from adb: %s", response)

    return response


def adb_pull(source_on_device, dest_on_computer):
    adb("pull", source_on_device, dest_on_computer)


def screenshot(dest_path):
    android_path = '/sdcard/screenshot-for-annotation.png'
    adb("shell", "screencap", "-p", android_path)
    adb_pull(android_path, dest_path)
    adb("shell", "rm", android_path)


def dumpui(dest_path):
    response = adb("shell", "uiautomator", "dump")
    pattern = re.compile(r"UI hierarchy dumped to: (.*\.xml)")
    match = pattern.findall(response.decode('utf-8'))

    if len(match) != 1:
        logging.error("Could not successfully dump UI state from Android device")
        error = """Failed to dump UI hierarchy
Output from ADB was not as expected.

Expected [sic]:
  UI hierchary dumped to: /storage/emulated/legacy/window_dump.xml

Received:
  %s
"""
        raise ValueError(error % response)

    adb_pull(match[0], dest_path)


def dump_info_from_device(screenshot_path, xml_ui_path, pretty_xml_ui_path = None):
    logging.info("Taking screenshot")
    screenshot(screenshot_path)

    logging.info("Dumping current state of UI on Android device")
    dumpui(xml_ui_path)

    if pretty_xml_ui_path is not None:
        logging.info("Beautifying UI XML")
        xml = minidom.parse(xml_ui_path)
        with open(pretty_xml_ui_path, "w") as out_file:
            out_file.write(xml.toprettyxml())


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    dump_info_from_device("/tmp/screenshot-for-annotation.png", "/tmp/screenshot.xml", "/tmp/screenshot.pretty.xml")